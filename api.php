<?php
/**
*转换接口
*BY∶云猫
*CC的小窝
*/
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $text = $_POST['text'];
    $type = $_POST['type'];
    $result = '';

    switch ($type) {
        case 'base64_encode':
            $result = base64_encode($text);
            break;
        case 'base64_decode':
            $result = base64_decode($text);
            break;
        case 'url_encode':
            $result = urlencode($text);
            break;
        case 'url_decode':
            $result = urldecode($text);
            break;
        case 'binary_encode':
            $result = '';
            for ($i = 0; $i < strlen($text); $i++) {
                $result .= sprintf("%08b", ord($text[$i])) . ' ';
            }
            $result = trim($result);
            break;
        case 'binary_decode':
            $result = '';
            $binaries = explode(' ', $text);
            foreach ($binaries as $binary) {
                $result .= chr(bindec($binary));
            }
            break;
        case 'unicode_encode':
            $result = '';
            for ($i = 0; $i < strlen($text); $i++) {
                $result .= '\u' . dechex(ord($text[$i]));
            }
            break;
        case 'unicode_decode':
            $result = preg_replace_callback('/\\\\u([0-9a-fA-F]{4})/', function ($matches) {
                return chr(hexdec($matches[1]));
            }, $text);
            break;
        default:
            $result = '未知的转换类型';
            break;
    }

    echo $result;
}
?>