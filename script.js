layui.use(['layer', 'form'], function() {
  var layer = layui.layer;

  document.getElementById('convertButton').addEventListener('click', function() {
    var inputText = document.getElementById('inputText').value;
    var conversionType = document.getElementById('conversionType').value;

    var xhr = new XMLHttpRequest();
    xhr.open('POST', 'api.php', true);
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    xhr.onreadystatechange = function() {
      if (xhr.readyState === 4 && xhr.status === 200) {
        document.getElementById('outputText').value = xhr.responseText;
        layer.msg('转换成功！');
      }
    };
    xhr.send('text=' + encodeURIComponent(inputText) + '&type=' + conversionType);
  });

  document.getElementById('copyButton').addEventListener('click', function() {
    var outputText = document.getElementById('outputText');
    outputText.select();
    document.execCommand('copy');
    layer.msg('复制成功！');
  });
});