# 文本编码解码

#### 介绍
支持在线把文本进行base64，Unicode，二进制，url编码和解码，界面采用layui

#### 软件架构
源码采用layui为框架


#### 安装教程

上传至服务器或者主机即可使用1，推荐php版本为72

演示：https://code.lwcat.cn/

项目地址https://gitee.com/ximami/code/ 